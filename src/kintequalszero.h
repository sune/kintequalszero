/**
beerware license
*/

#include <kintequalszero_export.h>

namespace KIntEqualsZero
{
KINTEQUALSZERO_EXPORT    extern const int value;
}
