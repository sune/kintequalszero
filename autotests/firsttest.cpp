/* This file is part of the KDE project
*/

#include <kintequalszero.h>

#include <QtTest/QtTest>


class TestIntEqualsZero : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void testCompareZero()
    {
        QCOMPARE(0, KIntEqualsZero::value);
    }

    void testNonZero_data()
    {
        QFETCH(int, value);
        QVERIFY(KIntEqualsZero::value != value);
    }
    void testNonZeroPositive_data()
    {
        QTest::addColumn<int>("value");
        QTest::newRow("one") << 1;
        QTest::newRow("two") << 2;
        QTest::newRow("three") << 3;
        QTest::newRow("four") << 4;
        QTest::newRow("five") << 5;
        QTest::newRow("six") << 6;

    }

};

QTEST_MAIN(TestIntEqualsZero)

#include <firsttest.moc>
